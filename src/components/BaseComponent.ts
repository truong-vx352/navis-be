import Vue, { defineComponent } from "vue";
import {
  getModel,
  deleteModel,
  putModel,
  postModel,
} from "app/src/services/service.api";
import { debounce, isEmpty } from "lodash";

export default defineComponent({
  name: "baseComponent",
  data: () => {
    return {
      model: <string>"",
      items: <any>[],
      fieldFilter: <any>{},
      itemSelected: <any>null,
      itemsSelected: <any>[],
      loading: <boolean>false,
      isShowModal: <boolean>false,
      form: <any>{},
      pagination: <any>{ rowsPerPage: 50, page: 1, last_page: 1 },
      exceptOnMounted: <any>[],
    };
  },

  mounted() {
    if (!this.exceptOnMounted.includes("index")) this.getItems();
  },

  methods: {
    async createItem(data: any | null = null) {
      const form = data || this.form;
      try {
        const res = await postModel(this.model, form);
        this.itemSelected = res.data.data;
        this.getItems();
        this.form = {};
        return true;
      } catch (error) {
        this.handleError(error);
      } finally {
        this.loading = false;
      }
    },

    async getItems() {
      try {
        const res = await this.fetchData();
        const { content } = res.data;
        this.items = content;
        this.setPagination(res.data);
      } catch (error) {
        this.handleError(error);
      } finally {
        this.loading = false;
      }
    },

    async pushItems() {
      try {
        const res = await this.fetchData();
        const { data } = res.data.data;
        const new_items = data || res.data.data;

        this.items = [...this.items, ...new_items];
        this.setPagination(res.data);
      } catch (e) {
        this.handleError(e);
      } finally {
        this.loading = false;
      }
    },

    async fetchData() {
      this.loading = true;
      const params = { ...this.fieldFilter };
      return await getModel(this.model, params);
    },

    setPagination(DataRes: any) {
      const { data } = DataRes;
      if (!data) return;

      this.pagination.last_page = data.last_page;
      this.pagination.page = data.current_page;
      this.pagination.rowsPerPage = data.per_page;
      this.pagination.from = data.from;
      this.pagination.to = data.to;
    },

    searchItems: debounce(function (this: any, seach = "") {
      this.fieldFilter.search = seach;
      this.fieldFilter.page = 1;
      this.getItems();
    }, 500),

    async updateItem(data: any = null, itemId: number | null = null) {
      const form = data || this.form;
      const id = itemId || this.itemSelected.id;

      try {
        const res = await putModel(this.model, form, id);
        this.getItems();
      } catch (error) {
        this.handleError(error);
      }
    },

    async deleteItem(id: number | null = null) {
      const itemId = id || this.itemSelected.id;

      try {
        const res = await deleteModel(this.model, itemId);
        this.getItems();
      } catch (error) {
        this.handleError(error);
      }
    },

    setLoading(boolean: boolean) {
      this.loading = boolean;
    },

    onChangePage(val: any) {
      this.items = val.data;
      this.pagination.page = val.current_page;
      this.pagination.rowsPerPage = val.per_page;
      this.pagination.last_page = val.last_page;
    },

    handleOnClickItem(val: any) {
      this.itemSelected = val;
    },

    handleFilterByStatus(id: number) {
      this.fieldFilter = { status: id };
      if (this.pagination.page === 1) {
        this.getItems();
      }
    },

    handleError(error: any) {
      return;
    },
  },
});
