import moment from "moment";

const formatMoney = new Intl.NumberFormat("vi", {
  style: "currency",
  currency: "VND",
});

export const moneyFormatter = (money) => {
  return formatMoney.format(money || 0);
};

export const moneyUnsignedFormatter = (money) => {
  if (money < 0) return formatMoney.format(0);
  return formatMoney.format(money || 0);
};

export const formatDate = (date, format = "DD/MM/YYYY") => {
  if (!date) return;
  return moment(date).format("DD/MM/YYYY");
};

export const formatDateTime = (date, format = "HH:mm DD/MM/YYYY") => {
  if (!date) return;
  return moment(date).format(format);
};
