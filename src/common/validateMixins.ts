export default {
  methods: {
    requiredRule(val: any) {
      if (!val) return "this field is required";
      return true;
    },
    phoneRule(val: any) {
      const phoneRegex = /(84|0[3|5|7|8|9])+([0-9]{8})\b/g;
      if (!phoneRegex.test(val))
        return "Vui lòng nhập số điện thoại đúng định dạng";
      return true;
    },
  },
};
