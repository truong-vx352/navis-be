export const RES_STATUS = {
  SUCCESS: 200,
};

export enum ROLE {
  ADMIN,
  USER,
}
