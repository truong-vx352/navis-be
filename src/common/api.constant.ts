const DOMAIN = process.env.BASE_URL;
const api = {
  DOMAIN,
  LOGIN: "auth/login",
  LOG_OUT: "auth/logout",
  CURRENT_USER: "auth/is-authenticated",
  CLASSROOM_LIST: "classroom/list",
};

export default api;
