import client from "src/services/client";
export const postModel = (model: string, data: any, headers?: any) => {
  return client({
    url: `${model}`,
    method: "POST",
    data,
  });
};
export const getModel = (model: string | undefined, params: any = {}) => {
  return client({
    url: `${model}`,
    method: "GET",
    params,
  });
};
export const headModel = (model: string, params: any) => {
  return client({
    url: `${model}`,
    method: "HEAD",
    params,
  });
};
export const putModel = (model: string, data: any, id?: any) => {
  return client({
    url: id ? `${model}/${id}` : `${model}`,
    method: "PUT",
    data,
  });
};
export const patchModel = (model: string, data: any, id: number) => {
  return client({
    url: `${model}/${id}`,
    method: "PATCH",
    data,
  });
};
export const deleteModel = (model: string, id: number) => {
  return client({
    url: `${model}/${id}`,
    method: "DELETE",
  });
};
