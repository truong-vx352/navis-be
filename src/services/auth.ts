import api from "src/common/api.constant";
import client from "src/services/client";

export const login = async (params: any): Promise<any> => {
  const res = await client.post<any>(api.LOGIN, null, { params });
  return res;
};

export const getCurrentUser = async (): Promise<any> => {
  const res = await client.get<any>(api.CURRENT_USER);
  return res.data;
};

export const refreshAccessToken = async (): Promise<any> => {
  const { data } = await client.post<any>("");
  // const { data } = await client.post<any>(api.REFRESH_TOKEN);
  return data;
};
