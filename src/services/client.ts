import axios, { AxiosError, AxiosResponse } from "axios";
import moment from "moment";
import api from "src/common/api.constant";
import route from "src/router";
import { useAuth } from "src/stores/auth";
import { debounce } from "lodash";

const client = axios.create({
  baseURL: api.DOMAIN,
});

client.interceptors.request.use(
  async (config) => {
    const { url } = config;
    const { data: dataAuth, refreshTokenSuccess, getAuthToken } = useAuth();
    let token = getAuthToken();
    // config.timeout = 8000;
    config.headers.Authorization = token ? `Bearer ${token}` : "";

    // if (url === api.REFRESH_TOKEN) {
    //   return config;
    // }

    if (dataAuth) {
      const expiredTime = moment(dataAuth?.data?.original.expire_at);
      const presentTime = moment();

      if (
        expiredTime.unix() - presentTime.unix() > 0 &&
        expiredTime.unix() - presentTime.unix() < 2 * 60
      ) {
        await refreshTokenSuccess();
        token = getAuthToken();
      }
    }

    return config;
  },
  (error) => Promise.reject(error)
);

const onResponse = (response: AxiosResponse): AxiosResponse => {
  return response;
};

const debounceNavigateToLogin = debounce(
  (router) => {
    router.replace("/login");
  },
  1000,
  { maxWait: 300 }
);

const onResponseError = (error: AxiosError): Promise<AxiosError> => {
  const status = error.response ? error.response.status : null;
  const data = error.response ? error.response.data : null;
  const { onLogout, setExPath } = useAuth();
  if (status === 401) {
    // @ts-ignore:next-line
    if (route.currentRoute.value.meta.requiredAuth)
      // @ts-ignore:next-line
      setExPath(route.currentRoute.value.path);
    debounceNavigateToLogin(route);
    onLogout();
  }
  return Promise.reject(error);
};

client.interceptors.response.use(onResponse, onResponseError);

export default client;
