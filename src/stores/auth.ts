import { getCurrentUser, refreshAccessToken } from "src/services/auth";
import { defineStore } from "pinia";
// import { logout } from "src/plugins/auth";
import { extend } from "quasar";

export const useAuth = defineStore("auth", {
  state: (): any => {
    return {
      user: JSON.parse(localStorage.getItem("user-data")!),
      data: null,
      exPath: null,
    };
  },
  getters: {
    is_authenticated(): boolean {
      return Boolean(this.user);
    },
  },
  actions: {
    async loginSuccess(dataLogin: any) {
      this.data = dataLogin;
      this.setToken(this.data?.token as string);
      await this.getCurrentAuthUser();
    },

    onLogout() {
      this.removeAuthenCookie();
      localStorage.removeItem("user-data");
      this.user = null;
      this.data = null;
      return 0;
    },
    async getCurrentAuthUser() {
      try {
        // get user data from api by token
        const res = await getCurrentUser();
        this.user = res;
        const copyOfData = extend(true /* deep */, {}, this.user);
        localStorage.setItem("user-data", JSON.stringify(copyOfData));
      } catch (error) {
        console.log(error);
      } finally {
      }
    },
    async refreshTokenSuccess() {
      try {
        // refresh token
        const res = await refreshAccessToken();
        this.setData(res);
        // localStorage.setItem(
        //   "token",
        //   this.data?.data.original.access_token as string
        // );
        this.setToken(this.data?.data.original.access_token as string);
      } catch (error) {
        console.log(error);
      }
    },

    setData(dataRes: any) {
      this.data = dataRes;
    },

    getAuthToken() {
      return localStorage.getItem("token");
    },

    setToken(token: string) {
      const expireTimeFiveMinutes = new Date(
        new Date().getTime() + 5 * 60 * 1000
      );
      localStorage.setItem("token", token);
    },

    removeAuthenCookie() {
      const expire = new Date(new Date().getTime() - 5 * 60 * 1000);
      document.cookie = "access_token=" + ";expires=" + expire + ";path =/ ";
    },

    setExPath(path: string | null) {
      this.exPath = path;
    },
  },
});
