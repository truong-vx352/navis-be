import { route } from "quasar/wrappers";
import {
  createRouter,
  createMemoryHistory,
  createWebHistory,
  createWebHashHistory,
  START_LOCATION,
} from "vue-router";
import routes from "./routes";
import { useAuth } from "src/stores/auth";
import { isEmpty } from "lodash";

/*
 * If not building with SSR mode, you can
 * directly export the Router instantiation;
 *
 * The function below can be async too; either use
 * async/await or return a Promise which resolves
 * with the Router instance.
 */

export default route(function (/* { store, ssrContext } */) {
  const createHistory = process.env.SERVER
    ? createMemoryHistory
    : process.env.VUE_ROUTER_MODE === "history"
    ? createWebHistory
    : createWebHashHistory;

  const Router: any = createRouter({
    scrollBehavior: () => ({ left: 0, top: 0 }),
    // @ts-ignore:next-line
    routes,

    // Leave this as is and make changes in quasar.conf.js instead!
    // quasar.conf.js -> build -> vueRouterMode
    // quasar.conf.js -> build -> publicPath
    history: createHistory(process.env.VUE_ROUTER_BASE),
  });

  Router.beforeEach(async (to, from, next) => {
    const authStore = useAuth();

    if (from === START_LOCATION && !isEmpty(authStore.user)) {
      authStore.getCurrentAuthUser();
    }
    // route này cần đăng nhập nhưng lại chưa
    if (to.meta.requiredAuth && isEmpty(authStore.user)) {
      authStore.setExPath(to.path);
      return next("/login");
    }

    // route này cần đăng nhập và đã đăng nhập sẵn rồi thì ko làm gì cả
    if (to.meta.requiredAuth && !isEmpty(authStore.user)) {
      let path = authStore.exPath;
      if (authStore.exPath) {
        authStore.setExPath(null);
        return next(path);
      }

      return next();
    }
    // route này đã đăng nhập rồi thì không được vào
    if (to.meta.requiredAuth === false && !isEmpty(authStore.user)) {
      return next("/");
    }

    return next();
  });
  return Router;
});
